package com.mamingchao.springcloud.eureka.controller;

import com.mamingchao.springcloud.eureka.api.ProviderApi2;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mamingchao on 2020/9/13.
 */
@Service
public class BusiService {

    @Autowired
    ProviderApi2 providerApi2;

    @HystrixCommand(fallbackMethod = "greetingFallBack")
    public String helloProcessLogic(){
        int i= 1/0;

        return providerApi2.hello("");

    }

    private String greetingFallBack() {
        return "You have come into hystrix command fallback method!";
    }
}
