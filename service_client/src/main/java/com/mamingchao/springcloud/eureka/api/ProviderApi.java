package com.mamingchao.springcloud.eureka.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by mamingchao on 2020/8/27.
 */
@FeignClient(name = "justaname",url = "http://localhost:8080")
public interface ProviderApi {

    @GetMapping("/getHi")
    String getHiProxy();

}
