package com.mamingchao.springcloud.nacos;

import com.alibaba.cloud.nacos.NacosConfigManager;
import com.alibaba.nacos.api.config.listener.AbstractListener;
import com.mamingchao.springcloud.nacos.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.io.StringReader;
import java.util.Properties;

@RefreshScope
@RestController
@SpringBootApplication
@EnableConfigurationProperties(User.class)
public class NacosDemo1Application {

    @Value("${user.name}")
    private String userName;

    @Value("${user.age}")
    private String userAge;

    @Value("${tddId}")
    private String tddId;

    @Autowired
    private User user;

    @Autowired
    private NacosConfigManager nacosConfigManager;

    @Bean
    public ApplicationRunner runner() {
        return applicationArguments -> {
            String dataId = "nacos-config-sample.properties";
            String group = "DEFAULT_GROUP";
            nacosConfigManager.getConfigService().addListener(dataId, group, new AbstractListener() {
                @Override
                public void receiveConfigInfo(String s) {
                    System.out.println("[Listener] " + s );
                    System.out.println("[before user]" + user);

                    try {
                        Properties properties = new Properties();
                        properties.load(new StringReader(s));
                        int age = Integer.valueOf(properties.getProperty("user.age"));
                        String name = properties.getProperty("user.name");

                        user.setAge(age);
                        user.setName(name);

                        System.out.println("[later user]" + user);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
        };
    }

    @PostConstruct
    public void init() {
        System.out.printf("[PostConstruct]user name is %s,user age is %s,and tddId is %s",userName,userAge,tddId);
    }


    @PreDestroy
    public void destroy() {
        System.out.printf("[destroy]The config from nacos config, user name is %s,user age is %s,and tddId is %s",userName,userAge,tddId);
    }

    @GetMapping
    public String testNacosConfigFresh() {
//        StringBuffer sb = new StringBuffer("The config from nacos config, user name is ");
//        sb.append(userName);
//        sb.append(",user age is ");
//        sb.append(userAge);
//
//        sb.append(",tddId is ");
//        sb.append(tddId);
//
//        return sb.toString();

        return user.toString();
    }

    public static void main(String[] args) {
        SpringApplication.run(NacosDemo1Application.class, args);
    }

}
