import java.util.ArrayList;
import java.util.List;

/**
 * Created by mamingchao on 2020/6/15.
 */
public class MainEnter {
    public static void main(String[] args) throws Throwable {
        //用list保持着引用 防止full gc回收常量池
        List<String> list = new ArrayList<String>();
        int i = 0;
        while(true){
            list.add(String.valueOf(i++).intern());
        }
    }

}
