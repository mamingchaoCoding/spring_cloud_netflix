package com.mamingchao.designmodel.factorymodel.normal_factory_model_2;

/**
 * Created by mamingchao on 2020/9/24.
 */
public class SingleSenderFactory {

    public Sender send(String type) {
        if ("mail".equals(type)) {
            return new MailSender();
        } else if ("sms".equals(type)) {
            return new SmsSender();
        } else {
            throw  new RuntimeException("请输入正确的类型");
        }
    }
}
