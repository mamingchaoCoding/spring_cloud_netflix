package com.mamingchao.designmodel.factorymodel.normal_factory_model_2;

/**
 * Created by mamingchao on 2020/9/24.
 */
public interface Sender {

    public boolean send(String content);
}
