package com.mamingchao.designmodel.factorymodel.normal_factory_model_2;

/**
 * Created by mamingchao on 2020/9/24.
 */
public class NormalFactoryTest {

    public static void main(String[] args) {
        SingleSenderFactory sf= new SingleSenderFactory();
        Sender sender = sf.send("mail");
        sender.send("hahahha");

        Sender sender1 = sf.send("Sms");
        sender1.send("heiheihei");
    }
}
