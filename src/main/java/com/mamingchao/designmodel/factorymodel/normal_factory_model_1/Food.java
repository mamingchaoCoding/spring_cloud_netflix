package com.mamingchao.designmodel.factorymodel.normal_factory_model_1;

/**
 * Created by mamingchao on 2020/7/22.
 */
public interface Food {

    public String getFoodName();

    public double getFoodPrice();



}
