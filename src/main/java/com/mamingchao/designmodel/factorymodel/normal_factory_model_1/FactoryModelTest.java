package com.mamingchao.designmodel.factorymodel.normal_factory_model_1;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by mamingchao on 2020/7/22.
 */
public class FactoryModelTest {

    public static void main(String[] args) {
        Food noodles = StaticFoodFactory.getFood("com.mamingchao.designmodel.factorymodel.Noodles");
        System.out.println(noodles.getFoodName());
        System.out.println(noodles.getFoodPrice());

        //这个地方 可以用 spring帮忙 生成 foodFactory
        DynamicFoodFactory foodFactory = new DynamicFoodFactory();
        Food rice = foodFactory.getFood("com.mamingchao.designmodel.factorymodel.Rice");
        System.out.println(rice.getFoodName());
        System.out.println(rice.getFoodPrice());

        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");

        Food dumplings = ctx.getBean("food",Food.class);
        System.out.println(dumplings.getFoodName());
        System.out.println(dumplings.getFoodPrice());

    }


}
