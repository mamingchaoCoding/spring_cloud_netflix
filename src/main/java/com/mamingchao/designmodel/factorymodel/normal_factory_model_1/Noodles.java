package com.mamingchao.designmodel.factorymodel.normal_factory_model_1;/**
 * Created by mamingchao on 2020/7/22.
 */

public class Noodles implements Food{

    @Override
    public String getFoodName() {
        return "I am noodles";
    }

    @Override
    public double getFoodPrice() {
        return 10;
    }
}
