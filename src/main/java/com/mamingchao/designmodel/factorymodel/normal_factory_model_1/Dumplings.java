package com.mamingchao.designmodel.factorymodel.normal_factory_model_1;

/**
 * Created by mamingchao on 2020/7/22.
 */

public class Dumplings implements Food{

    @Override
    public String getFoodName() {
        return "I am dumplings";
    }

    @Override
    public double getFoodPrice() {
        return 15;
    }
}
