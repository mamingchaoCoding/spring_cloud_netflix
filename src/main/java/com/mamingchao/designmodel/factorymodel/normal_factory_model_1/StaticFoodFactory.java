package com.mamingchao.designmodel.factorymodel.normal_factory_model_1;

/**
 * Created by mamingchao on 2020/7/22.
 */
public class StaticFoodFactory {

    public static Food getFood(String foodName){
        try {
            Class<Food> aClass = (Class<Food>) Class.forName(foodName);
            return aClass.newInstance();

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }

        return null;
    }
}
