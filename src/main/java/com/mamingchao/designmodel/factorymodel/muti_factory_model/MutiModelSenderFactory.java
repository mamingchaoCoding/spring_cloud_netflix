package com.mamingchao.designmodel.factorymodel.muti_factory_model;

import com.mamingchao.designmodel.factorymodel.normal_factory_model_2.MailSender;
import com.mamingchao.designmodel.factorymodel.normal_factory_model_2.Sender;
import com.mamingchao.designmodel.factorymodel.normal_factory_model_2.SmsSender;

/**
 * Created by mamingchao on 2020/9/24.
 */
public class MutiModelSenderFactory {

    public Sender getMailSenderInstance() {
        return new MailSender();
    }


    public Sender getSmsSenderInstance() {
        return new SmsSender();
    }

}
