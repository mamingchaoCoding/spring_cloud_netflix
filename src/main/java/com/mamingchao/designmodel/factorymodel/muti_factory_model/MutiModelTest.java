package com.mamingchao.designmodel.factorymodel.muti_factory_model;

import com.mamingchao.designmodel.factorymodel.normal_factory_model_2.Sender;

/**
 * Created by mamingchao on 2020/9/24.
 */
public class MutiModelTest {

    public static void main(String[] args) {
        MutiModelSenderFactory mmsf = new MutiModelSenderFactory();

        Sender sender = mmsf.getMailSenderInstance();

        sender.send("muti factory test");
    }

}
