package com.mamingchao.designmodel.singleton_model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 单例模式的扩展
 * 有限的多例模式
 *
 * Created by mamingchao on 2020/9/24.
 */
public class MultitcmModel {

    final static int instanceNum = 5;

    static List<Multition> lists = new ArrayList<>();

    static {
        for (int i = 0; i < instanceNum; i++) {
            lists.add(new Multition());
        }
    }


    public static Multition getRandomInstance() {
        int num = new Random().nextInt(instanceNum);

        System.out.println("The random num is " + num);
        return lists.get(num);
    }


    public static void main(String[] args) {
        Multition obj1 = MultitcmModel.getRandomInstance();
        Multition obj2 = MultitcmModel.getRandomInstance();
        Multition obj3 = MultitcmModel.getRandomInstance();

        System.out.println(obj1 == obj2);
        System.out.println(obj3 == obj2);
        System.out.println(obj1 == obj3);


//        for (int i = 0; i <100 ; i++) {
//            int num = new Random().nextInt(3);
//
//            System.out.println("The random num is " + num);
//        }

    }

}
