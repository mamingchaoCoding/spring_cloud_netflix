package com.mamingchao.springcloud.eureka.controller;

import com.mamingchao.springcloud.eureka.service.HealthServiceStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.QueryParam;

/**
 * Created by mamingchao on 2020/8/24.
 */
@RestController
public class HealthController {

    @Autowired
    HealthServiceStatus statusService;



    @GetMapping("status")
    public String maintance(@QueryParam("activite") boolean activite){

        this.statusService.setStatus(activite);

        String result = "";

        if (activite) {
            result = "server 接到启动通知";
        } else {
            result = "server 接到启动通知";
        }

        return result;
    }
}
