package com.mamingchao.springcloud.eureka.service;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Service;

/**
 * Created by mamingchao on 2020/8/24.
 */
@Service
public class HealthServiceStatus implements HealthIndicator{

    private boolean status;

    @Override
    public Health health() {

        if (status)
            return new Health.Builder().up().build();
        return new Health.Builder().down().build();
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
