package com.mamingchao.springcloud.eureka.controller;

import com.mamingchao.springcloud.eureka.api.UserApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by mamingchao on 2020/8/21.
 */
@RestController
public class Test2Controller implements UserApi{

    @Value("${server.port}")
    private String port;
    private AtomicInteger counter = new AtomicInteger();

    @Override
    public String greeting() {

        return "Greeting for service provider 2";
    }

    @Override
    public String hello(String name) {
        int i =1/0;
        return "Say hello to " + name;
    }

    /**
     * ribbonReadTimeoutTest
     * @return
     */
    @Override
    public String ribbonReadTimeoutTest() {


        try {
            Thread.sleep(1000);
//            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        System.out.println("This is the " + counter.incrementAndGet() + " times invoke provicer 2, method ribbonReadTimeoutTest");
        System.out.println("Current timestamp is "+ System.currentTimeMillis());
        return "My server port is "+ port + "and this is ribbon read timeout test!";
    }
}
