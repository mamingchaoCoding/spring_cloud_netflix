package com.mamingchao.springcloud.session;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Created by mamingchao on 2020/9/18.
 */
@Controller
public class TestController {

    @GetMapping("/hei")
    public String phishing(){
        return "phishing";
    }
}
